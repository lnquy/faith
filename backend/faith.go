package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/alexedwards/scs/engine/memstore"
	"github.com/alexedwards/scs/session"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	cfg "github.com/lnquy/faith/backend/conf"
	"github.com/lnquy/faith/backend/models"
	glb "github.com/lnquy/faith/backend/modules/global"
	l "github.com/lnquy/faith/backend/modules/logger"
	mdw "github.com/lnquy/faith/backend/modules/middlewares"
	"github.com/lnquy/faith/backend/routers"
	"github.com/lnquy/faith/backend/utils"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"
	"time"
)

var (
	// Flags
	fIp           *string
	fPort         *string
	fMode         *string
	fHTTPS        *bool
	fTLSCert      *string
	fTLSKey       *string
	fReadTimeout  *int
	fWriteTimeout *int
	fMaxCPUs      *int
)

func init() {
	fIp = flag.String("ip", "", "Server IP address (E.g: 10.10.1.123, 192.168.1.100...)")
	fPort = flag.String("port", "", "Server port (E.g: 8080, 8443...)")
	fMode = flag.String("run", "", "Running mode (production or development)")
	fHTTPS = flag.Bool("https", true, "Enable HTTPS server")
	fTLSCert = flag.String("tlscert", "", "Path to TLS certification file (*.crt, *.pem, *.cert...)")
	fTLSKey = flag.String("tlskey", "", "Path to TLS key file (*.key)")
	fReadTimeout = flag.Int("read", 0, "Maximum duration in second to read the entire request")
	fWriteTimeout = flag.Int("write", 0, "Maximum duration in second to write to the response")
	fMaxCPUs = flag.Int("cpus", 0, "Maximum CPU cores will be used by server")
}

func main() {
	// Configuration
	cfg.ReadFromFile("") // Continue to run with default config if failed to decode config file
	cliConfig()          // CLI configs will override the file configs
	if err := cfg.WriteToFile(""); err != nil {
		log.Errorf("Cannot write configuration to file: %s", err)
	}

	// TODO: Remove later
	routers.LoadIndexTags()

	// Server
	r := chi.NewRouter()
	configureServer(r)

	// Session
	engine := memstore.New(12 * time.Hour)
	sessionMgr := session.Manage(engine,
		session.IdleTimeout(7*24*time.Hour), // TODO: Config
		session.Lifetime(7*24*time.Hour),
		session.Persist(true),
		session.Secure(cfg.Global.Server.EnableHTTPS),
	)

	// Handle os signals for graceful shutdown server
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT, os.Interrupt, os.Kill)

	// Server
	addr := fmt.Sprintf("%s:%s", cfg.Global.Server.Ip, cfg.Global.Server.Port)
	server := &http.Server{
		Addr:         addr,
		Handler:      sessionMgr(cors.Default().Handler(r)), // CORS handler
		ReadTimeout:  time.Duration(cfg.Global.Server.ReadTimeout) * time.Second,
		WriteTimeout: time.Duration(cfg.Global.Server.WriteTimeout) * time.Second,
	}

	go func() {
		// Start server
		if cfg.Global.Server.EnableHTTPS {
			if err := utils.CheckTLSCertificate(); err != nil {
				log.Panic(err)
			}
			if cfg.Global.Server.Port == glb.DefaultHTTPPort {
				cfg.Global.Server.Port = glb.DefaultHTTPSPort
				addr = fmt.Sprintf("%s:%s", cfg.Global.Server.Ip, cfg.Global.Server.Port)
				server.Addr = addr
			}

			// [?] The process will be blocked if we call go log.Error(...) directly here
			// Need to investigate more
			go func() {
				subAddr := fmt.Sprintf("%s:%s", cfg.Global.Server.Ip, glb.DefaultHTTPPort)
				log.Infof("Sub HTTP redirection server is serving at http://%s/", subAddr)
				log.Error(http.ListenAndServe(subAddr, http.HandlerFunc(httpRedirect)))
			}()

			log.Infof("Main server is serving at https://%s/", addr)
			log.Fatal(server.ListenAndServeTLS(cfg.Global.Server.TLSCert, cfg.Global.Server.TLSKey))
		} else {
			log.Infof("Server is serving at http://%s/", addr)
			log.Fatal(server.ListenAndServe())
		}
	}()

	// Graceful shutdown
	<-stop
	log.Info("Shutting down the server...")
	ctx, _ := context.WithTimeout(context.Background(), 3*time.Second)
	server.Shutdown(ctx)
	log.Info("Have a nice day, goodbye!")

	if cfg.Global.Server.RunMode == glb.Production { // [?] May need more works here
		l.FileLogger.Sync()
		l.FileLogger.Close() // Grateful close log file writer
	}
}

func cliConfig() {
	flag.Parse()

	// Override file configurations
	if *fIp != "" {
		cfg.Global.Server.Ip = *fIp
	}
	if *fPort != "" {
		cfg.Global.Server.Port = *fPort
	}
	if *fMode != "" {
		cfg.Global.Server.RunMode = *fMode
	}
	if *fHTTPS != cfg.Global.Server.EnableHTTPS {
		cfg.Global.Server.EnableHTTPS = *fHTTPS
	}
	if *fTLSCert != "" {
		cfg.Global.Server.TLSCert = *fTLSCert
	}
	if *fTLSKey != "" {
		cfg.Global.Server.TLSKey = *fTLSKey
	}
	if *fReadTimeout > 0 {
		cfg.Global.Server.ReadTimeout = *fReadTimeout
	}
	if *fWriteTimeout > 0 {
		cfg.Global.Server.WriteTimeout = *fWriteTimeout
	}
	if *fMaxCPUs > 0 {
		cfg.Global.Server.MaxCPUs = *fMaxCPUs
	}
}

func configureServer(r *chi.Mux) {
	// Log configuration
	if cfg.Global.Server.RunMode == glb.Development {
		l.InitDevLogger()

		//reqLogger := log.New()
		//reqLogger.Level = log.DebugLevel
		//reqLogger.Formatter = l.GetLogFormatter()
		//reqLogger.Out = os.Stdout
		//r.Use(mdw.NewStructuredLogger(reqLogger))
		r.Use(middleware.Logger)

		utils.PrintBanner()
		log.Infof("Server run mode: %s", glb.Development)
		log.Warn("Remember to switch to \"production\" mode when deploy on real server (-run=production)")
	} else {
		if err := l.InitProdLogger(); err != nil {
			log.Errorf("Init production logger failed: %s", err)
		}
		utils.PrintBanner()
		log.Infof("Server run mode: %s", glb.Production)
	}

	// Allocate server resource
	runtime.GOMAXPROCS(cfg.Global.Server.MaxCPUs)
	cfg.Global.Server.MaxCPUs = utils.GetMaxParallelism()
	log.Infof("Max CPU(s) runs in parallel: %d", cfg.Global.Server.MaxCPUs)

	// Database
	initDatabase()

	// Middleware
	if cfg.Global.Perf.EnableCompression {
		r.Use(mdw.DefaultCompress) // Custom middleware to compress popular content types
	}
	if cfg.Global.Perf.EnableCache {
		r.Use(mdw.CacheControl) // [?] Allow browser to cache contents and re-validate if content changed
	}
	r.Use(middleware.Recoverer)

	// Static files routes
	wd, err := os.Getwd()
	if err != nil {
		log.Panic(err)
	}
	dir := filepath.Join(wd, "templates", "static")
	FileServer(r, "/static", http.Dir(dir))
	dir = filepath.Join(wd, "data") // User data
	FileServer(r, "/data", http.Dir(dir))
	dir = filepath.Join(wd, "templates", "astatic")
	FileServer(r, "/astatic", http.Dir(dir))

	// Public routes
	r.NotFound(routers.NotFound)
	r.Get("/", routers.GetIndex)
	r.Get("/favicon*", routers.GetFavIcon)
	r.Get("/login", routers.GetLogin)
	r.Post("/login", routers.PostLogin)
	r.Get("/admin", routers.GetAdmin)
	// Front page
	r.Get("/posts/{pid}", routers.GetPostPage)
	r.Get("/authors/{author_name}", routers.GetAuthorPage)
	r.Get("/tags/{tag}", routers.GetTagPage)

	// APIs
	r.Route("/api/v1", func(r chi.Router) {
		// Public APIs
		r.Group(func(r chi.Router) {
			r.Get("/posts", routers.GetPostsList) // /posts?offset=2&tag=go
			r.Get("/posts/{pid}", routers.GetPost)
		})

		// Secured APIs
		r.Group(func(r chi.Router) {
			r.Use(mdw.SessionAuth)
			// Media
			r.Post("/media", routers.UploadMedia)
			r.Get("/media", routers.GetAllMedia)
			r.Delete("/media/{mid}", routers.DeleteMedia)
			// Post
			r.Post("/posts", routers.CreatePost)
			r.Delete("/posts/{pid}", routers.DeletePost)
			// Users
			//r.Post("/users") // Register
			//r.Get("/users") // Get all users
			r.Get("/users/{uid}", routers.GetFullUserInfo)
			//r.Post("/user/{uid}/avatar") // Change avatar
			r.Put("/users/{uid}/pw", routers.ChangePassword)
			r.Put("/users/{uid}/profile", routers.UpdateUser)
			r.Put("/users/{uid}/avatar", routers.UpdateUser)
			r.Put("/users/{uid}/info", routers.UpdateUser)
			r.Put("/users/{uid}/authority", routers.UpdateUser)
			r.Get("/users/{uid}/logout", routers.Logout)
		})
	})

	// TODO: Test
	r.Get("/author", routers.GetAuthor)
}

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	fs := http.StripPrefix(path, http.FileServer(root))
	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", http.StatusMovedPermanently).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}

func initDatabase() {
	dbPath, err := os.Getwd()
	if err != nil {
		log.Panicf("Cannot detect current working directory! %s", err)
	}
	dbPath = filepath.Join(dbPath, "data", "db")
	log.Infof("DB path: %s", dbPath)
	if err := models.MigrateDatabases(dbPath); err != nil {
		log.Panicf("Failed to migrate databases! %s", err) // TODO: Allow admin to configure via web GUI
	}
}

func httpRedirect(w http.ResponseWriter, req *http.Request) {
	target := "https://" + req.Host + ":" + cfg.Global.Server.Port + req.URL.Path
	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	http.Redirect(w, req, target, http.StatusTemporaryRedirect)
}
