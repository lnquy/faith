package routers

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/render"
	cfg "github.com/lnquy/faith/backend/conf"
	"github.com/lnquy/faith/backend/models"
	glb "github.com/lnquy/faith/backend/modules/global"
	"github.com/lnquy/faith/backend/utils"
	log "github.com/sirupsen/logrus"
	tmpl "html/template"
	"net/http"
	"strconv"
	"strings"
)

type postResp struct {
	Title       string
	Post        models.Post
	RawContent  tmpl.HTML
	PublishDate string
}

func GetPostPage(w http.ResponseWriter, r *http.Request) {
	if parm := chi.URLParam(r, "pid"); parm == "" {
		http.Error(w, "Invalid postId", http.StatusBadRequest)
	} else {
		pid, err := strconv.Atoi(parm)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if cfg.Global.Server.RunMode == glb.Development { // Always reload template data in development mode
			if err := loadPostTemplate(""); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		if postDataLen <= 0 { // Template parse failed
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if post, err := models.GetPost(uint(pid)); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			publishDate := post.PublishDate.Format("Jan 2 2006")
			if post.ImgSrc == "" {
				post.ImgSrc = "/static/img/about-bg.jpg"
			}
			data := &postResp{
				Title:       post.Title,
				Post:        post,
				RawContent:  tmpl.HTML(post.Content),
				PublishDate: publishDate,
			}

			if err = postTmpl.Execute(w, data); err != nil {
				log.Error(err)
			} else {
				if p, ok := w.(http.Pusher); ok { // HTTP/2
					for _, v := range postStatics {
						log.Debugf("HTTP2 pushed: %s", v)
						p.Push(v, nil)
					}
				}
			}
		}
	}
}

func CreatePost(w http.ResponseWriter, r *http.Request) {
	p := &models.Post{}
	if err := utils.DecodeJSON(r, p); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := p.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	uid := utils.GetAccessUId(r)
	p.OwnerID = uid

	if p.PostID == 0 { // Post never be saved, save to Posts table first
		if err := p.Create(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if p.Draft { // If it's draft post then save to Drafts table, too
			if d, err := models.CreateDraftFrom(p); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			} else {
				render.JSON(w, r, d)
				return
			}
		}
	} else { // Post already saved on Posts table
		if p.Draft { // Update post in Posts to the latest version of draft if its state is drafting
			if _, err := models.CreateDraftFrom(p); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			} else {
				if err := p.UpdateLatestDraft(); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			}
		} else { // Update current post in Posts
			if err := p.Update(); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
	}

	render.JSON(w, r, p)
}

func GetPostsList(w http.ResponseWriter, r *http.Request) {
	offset := glb.DefaultOffset
	oParm := r.URL.Query().Get("offset")
	if oParm != "" {
		var err error
		if offset, err = strconv.Atoi(oParm); err != nil {
			offset = glb.DefaultOffset
		}
	}

	tagParm := strings.TrimSpace(r.URL.Query().Get("tag"))
	var posts []models.Post
	var err error
	if tagParm == "" {
		if posts, err = models.GetPostsList(cfg.Global.Post.LimitPerPage, offset*cfg.Global.Post.LimitPerPage); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		if posts, err = models.GetPostsListByTag(tagParm, cfg.Global.Post.LimitPerPage, offset*cfg.Global.Post.LimitPerPage); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	render.JSON(w, r, posts)
}


func GetPost(w http.ResponseWriter, r *http.Request) {
	if parm := chi.URLParam(r, "pid"); parm == "" {
		http.Error(w, "Invalid postId", http.StatusBadRequest)
	} else {
		if pid, err := strconv.Atoi(parm); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		} else if post, err := models.GetPost(uint(pid)); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			render.JSON(w, r, post)
		}
	}
}

func DeletePost(w http.ResponseWriter, r *http.Request) {
	if parm := chi.URLParam(r, "pid"); parm == "" {
		http.Error(w, "Invalid postId", http.StatusBadRequest)
	} else {
		uid := utils.GetAccessUId(r)
		pid, err := strconv.Atoi(parm)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		p := models.Post{
			PostID: uint(pid),
			PostMeta: models.PostMeta{
				OwnerID: uid,
			},
		}

		if !p.CheckPostOwner() {
			http.Error(w, "Permission requires to delete this post postId", http.StatusForbidden)
			return
		}

		if err := p.Delete(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			render.JSON(w, r, glb.OkMsg)
		}
	}
}
