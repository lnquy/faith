package routers

import (
	"github.com/alexedwards/scs/session"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func GetAdmin(w http.ResponseWriter, r *http.Request) {
	if uid, err := session.GetInt(r, "uid"); err != nil || (err == nil && uid == 0) {
		// Not logged in, redirect to login page
		http.Redirect(w, r, "/login", http.StatusMovedPermanently)
		return
	}

	if adminDataLen != 0 {
		w.Write(adminData)
		p, ok := w.(http.Pusher) // HTTP/2
		if ok {
			for _, v := range adminStatics {
				log.Debugf("HTTP2 pushed: %v", v)
				p.Push(v, nil)
			}
		}
	} else {
		NotFound(w, r)
	}
}
