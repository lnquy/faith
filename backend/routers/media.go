package routers

import (
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/render"
	"github.com/lnquy/faith/backend/models"
	"github.com/lnquy/faith/backend/utils"
	"github.com/vincent-petithory/dataurl"
	"net/http"
	"path"
	"strings"
	glb "github.com/lnquy/faith/backend/modules/global"
)

func UploadMedia(w http.ResponseWriter, r *http.Request) {
	raw := &models.RawMedia{}
	if err := utils.DecodeJSON(r, raw); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := raw.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	uid := utils.GetAccessUId(r)
	raw.OwnerID = uid
	sPath := path.Join("data", "users", fmt.Sprintf("%d", uid), raw.Type)
	if err := utils.EnsurePath(sPath); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sPath = path.Join(sPath, raw.Filename)
	raw.StoragePath = sPath
	raw.WebPath = path.Join("..", sPath) // Relative path, root is templates folder
	if data, err := dataurl.DecodeString(raw.Data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		// TODO: Thumbnail image
		if err := raw.MediaMetadata.Upload(data.Data); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			render.JSON(w, r, raw.MediaMetadata)
		}
	}
}

func GetAllMedia(w http.ResponseWriter, r *http.Request) {
	// TODO: Get media by userID
	if media, err := models.GetAllMedia(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		render.JSON(w, r, media)
	}
}

func DeleteMedia(w http.ResponseWriter, r *http.Request) {
	midList := strings.Split(chi.URLParam(r, "mid"), ",")

	if err := models.DeleteBatchMedia(midList); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		render.JSON(w, r, glb.OkMsg)
	}
}
