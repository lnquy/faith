package routers

import (
	"github.com/alexedwards/scs/session"
	"github.com/go-chi/chi/render"
	"github.com/lnquy/faith/backend/models"
	glb "github.com/lnquy/faith/backend/modules/global"
	"github.com/lnquy/faith/backend/utils"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func GetLogin(w http.ResponseWriter, r *http.Request) {
	if uid, err := session.GetInt(r, "uid"); err == nil && uid != 0 {
		// Already logged in, redirect to admin page
		http.Redirect(w, r, "/admin", http.StatusMovedPermanently)
		return
	}

	if loginDataLen != 0 {
		w.Write(loginData)
		p, ok := w.(http.Pusher) // HTTP/2
		if ok {
			for _, v := range loginStatics {
				log.Debugf("HTTP2 pushed: %v", v)
				p.Push(v, nil)
			}
		}
	} else {
		NotFound(w, r)
	}
}

type LoginForm struct {
	Username string `json:"username"`
	Password string `json:"password"` // Auto remember the logged in user
}

// PostLogin verify user login and return new token and session if login successfully
func PostLogin(w http.ResponseWriter, r *http.Request) {
	form := &LoginForm{}
	if err := utils.DecodeJSON(r, form); err != nil {
		log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	uc := models.UserAccount{
		Username: form.Username,
		Password: form.Password,
	}

	if err := uc.ValidateLoginData(); err != nil {
		log.Errorf("Invalid login data! %s", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if user, err := uc.Login(); err != nil {
		log.Errorf("Login failed: %s", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
	} else {
		models.UpdateLastActive(user.UserID) // [?] Omit error
		log.Infof("User logged in: %v", user.Username)
		session.PutInt(r, "uid", int(user.UserID))
		session.PutString(r, "username", user.Username)
		session.PutObject(r, "authority", user.Authority)
		render.JSON(w, r, glb.OkMsg)
	}
}
