package routers

import (
	cfg "github.com/lnquy/faith/backend/conf"
	"github.com/lnquy/faith/backend/models"
	glb "github.com/lnquy/faith/backend/modules/global"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

type indexResp struct {
	Title     string
	Tags      []string
	Posts     []models.Post
	HeaderImg string
}

var indexTags []string

func LoadIndexTags() {
	if strings.TrimSpace(cfg.Global.Temp.PostTags) == "" {
		indexTags = []string{}
		return
	}
	indexTags = strings.Split(cfg.Global.Temp.PostTags, ",")
	for i, v := range indexTags {
		indexTags[i] = strings.ToLower(strings.TrimSpace(v))
	}
}

func GetIndex(w http.ResponseWriter, r *http.Request) {
	if posts, err := models.GetPostsList(cfg.Global.Post.LimitPerPage, glb.DefaultOffset); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		if cfg.Global.Server.RunMode == glb.Development { // Always reload template data in development mode
			if err := loadIndexTemplate(""); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		if indexDataLen <= 0 { // Template parse failed
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		data := &indexResp{
			Title:     "Index page",
			Tags:      indexTags,
			Posts:     posts,
			HeaderImg: "/static/img/home-bg.jpg",
		}
		if err = indexTmpl.Execute(w, data); err != nil {
			log.Error(err)
		} else {
			if p, ok := w.(http.Pusher); ok { // HTTP/2
				for _, v := range indexStatics {
					log.Debugf("HTTP2 pushed: %s", v)
					p.Push(v, nil)
				}
			}
		}
	}
}

func GetFavIcon(w http.ResponseWriter, r *http.Request) {
	log.Debugln("favicon")
	w.Write(favIconData)
}
