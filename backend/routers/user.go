package routers

import (
	"github.com/alexedwards/scs/session"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/render"
	"github.com/lnquy/faith/backend/models"
	glb "github.com/lnquy/faith/backend/modules/global"
	"github.com/lnquy/faith/backend/utils"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
	"strings"
	"github.com/pkg/errors"
)

func GetFullUserInfo(w http.ResponseWriter, r *http.Request) {
	if parm := chi.URLParam(r, "uid"); parm == "" {
		http.Error(w, "Invalid postId", http.StatusBadRequest)
	} else {
		uid, err := strconv.Atoi(parm)
		if err != nil {
			http.Error(w, "Invalid postId", http.StatusBadRequest)
		}
		if user, err := models.GetUserByUID(uint(uid)); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			render.JSON(w, r, user)
		}
	}
}

type PasswordForm struct {
	CurPass string `json:"cur_pass"`
	NewPass string `json:"new_pass"`
}
func (pf *PasswordForm) validate() error {
	if len(pf.CurPass) < 8 || len(pf.CurPass) > 30 || len(pf.NewPass) < 8 || len(pf.NewPass) > 30 {
		return errors.New("Invalid password: Length from 8 to 30 characters")
	}
	return nil
}

func ChangePassword(w http.ResponseWriter, r *http.Request) {
	pwForm := &PasswordForm{}
	if err := utils.DecodeJSON(r, pwForm); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := pwForm.validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	uid := utils.GetAccessUId(r)
	if user, err := models.CheckPassword(uid, pwForm.CurPass); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	} else {
		var bHash []byte
		if bHash, err = bcrypt.GenerateFromPassword([]byte(pwForm.NewPass), bcrypt.DefaultCost); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		user.Password = string(bHash[:])
		if err := user.UpdatePassword(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			render.JSON(w, r, glb.OkMsg)
		}
	}
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	var parm string
	if parm = chi.URLParam(r, "uid"); parm == "" {
		http.Error(w, "Invalid userId", http.StatusBadRequest)
		return
	}
	uid, err := strconv.Atoi(parm)
	if err != nil {
		http.Error(w, "Invalid userId", http.StatusBadRequest)
		return
	}

	user := &models.User{}
	if err = utils.DecodeJSON(r, user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user.UserID = uint(uid)
	uType := r.URL.String()
	if strings.Contains(uType, "profile") {
		err = user.UpdateProfile()
	} else if strings.Contains(uType, "info") {
		err = user.UpdateInfo()
	} else if strings.Contains(uType, "avatar") {
		err = user.UpdateAvatar()
	} else if strings.Contains(uType, "authority") {
		err = user.UpdateAuthority()
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		render.JSON(w, r, glb.OkMsg)
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	var parm string
	if parm = chi.URLParam(r, "uid"); parm == "" {
		http.Error(w, "Invalid userId", http.StatusBadRequest)
		return
	}
	uid, err := strconv.Atoi(parm)
	if err != nil {
		http.Error(w, "Invalid userId", http.StatusBadRequest)
		return
	}
	models.UpdateLastActive(uint(uid)) // [?] Omit error

	session.Destroy(w, r)
	render.JSON(w, r, glb.OkMsg)
}
