package routers

import (
	"github.com/lnquy/faith/backend/models"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type tagResp struct {
	Title     string
	Posts     []models.Post
	HeaderImg string
}

func GetTagPage(w http.ResponseWriter, r *http.Request) {
	if err := tagTmpl.Execute(w, nil); err != nil {
		log.Error(err)
	} else {
		if p, ok := w.(http.Pusher); ok { // HTTP/2
			for _, v := range tagStatics {
				log.Debugf("HTTP2 pushed: %s", v)
				p.Push(v, nil)
			}
		}
	}

	//if posts, err := models.GetPostsList(cfg.Global.Post.LimitPerPage, glb.DefaultOffset); err != nil {
	//	http.Error(w, err.Error(), http.StatusInternalServerError)
	//} else {
	//	if cfg.Global.Server.RunMode == glb.Development { // Always reload template data in development mode
	//		if err := loadIndexTemplate(""); err != nil {
	//			http.Error(w, err.Error(), http.StatusInternalServerError)
	//			return
	//		}
	//	}
	//	if indexDataLen <= 0 { // Template parse failed
	//		http.Error(w, err.Error(), http.StatusInternalServerError)
	//		return
	//	}
	//
	//	data := &indexResp{
	//		Title:     "Index page",
	//		Posts:     posts,
	//		HeaderImg: "/static/img/home-bg.jpg",
	//	}
	//	if err = indexTmpl.Execute(w, data); err != nil {
	//		log.Error(err)
	//	} else {
	//		if p, ok := w.(http.Pusher); ok { // HTTP/2
	//			for _, v := range indexStatics {
	//				log.Debugf("HTTP2 pushed: %s", v)
	//				p.Push(v, nil)
	//			}
	//		}
	//	}
	//}
}
