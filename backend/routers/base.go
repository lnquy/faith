package routers

import (
	"bytes"
	glb "github.com/lnquy/faith/backend/modules/global"
	"github.com/lnquy/faith/backend/utils"
	log "github.com/sirupsen/logrus"
	tmpl "html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

var (
	// Index page
	indexTmpl    *tmpl.Template
	indexDataLen int
	indexStatics []string

	// Index page
	postTmpl    *tmpl.Template
	postDataLen int
	postStatics []string

	// Login page
	loginData    []byte
	loginDataLen int
	loginStatics []string

	// Author page
	authorTmpl    *tmpl.Template
	authorDataLen int
	authorStatics []string

	// Tag page
	tagTmpl    *tmpl.Template
	tagDataLen int
	tagStatics []string

	// Admin page
	adminData    []byte
	adminDataLen int
	adminStatics []string

	// Favicon
	favIconData []byte

	// 404 Not found page
	notFoundData    []byte
	notFoundStatics []string
)

func init() {
	if err := loadIndexTemplate(""); err != nil {
		log.Panicf("Cannot load index template: %s", err)
	}
	if err := loadPostTemplate(""); err != nil {
		log.Panicf("Cannot load post template: %s", err)
	}
	if err := loadAuthorTemplate(""); err != nil {
		log.Panicf("Cannot load author template: %s", err)
	}
	if err := loadTagTemplate(""); err != nil {
		log.Panicf("Cannot load tag template: %s", err)
	}
	if err := loadTemplateData(); err != nil {
		log.Panicf("Cannot load other templates data: %s", err)
	}
}

func loadIndexTemplate(indexPath string) error {
	indexDataLen = 0

	if indexPath == "" {
		indexPath = glb.DefaultIndexTmplPath
	}
	tmp, err := ioutil.ReadFile(indexPath)
	if err != nil {
		return err
	}
	tmplLinks := append(utils.GetAllNestedTemplates(tmp, glb.NestedTmplRegex), indexPath)
	indexTmpl, err = tmpl.New("index.html").ParseFiles(tmplLinks...)
	if err != nil {
		return err
	}

	var indexData bytes.Buffer
	if err = indexTmpl.Execute(&indexData, nil); err != nil {
		return err
	}

	indexStatics = utils.GetAllStaticLinks(indexData.Bytes(), glb.StaticRegex)
	indexDataLen = indexData.Len()
	return nil
}

func loadPostTemplate(postPath string) (err error) {
	postDataLen = 0

	if postPath == "" {
		postPath = glb.DefaultPostTmplPath
	}
	tmp, err := ioutil.ReadFile(postPath)
	if err != nil {
		return err
	}
	tmplLinks := append(utils.GetAllNestedTemplates(tmp, glb.NestedTmplRegex), postPath)
	postTmpl, err = tmpl.New("post.html").ParseFiles(tmplLinks...)
	if err != nil {
		return err
	}

	var postData bytes.Buffer
	if err = postTmpl.Execute(&postData, nil); err != nil {
		return
	}
	postStatics = utils.GetAllStaticLinks(postData.Bytes(), glb.StaticRegex)
	postDataLen = postData.Len()
	return nil
}

func loadAuthorTemplate(authorPath string) (err error) {
	authorDataLen = 0

	if authorPath == "" {
		authorPath = glb.DefaultAuthorTmplPath
	}
	tmp, err := ioutil.ReadFile(authorPath)
	if err != nil {
		return err
	}
	tmplLinks := append(utils.GetAllNestedTemplates(tmp, glb.NestedTmplRegex), authorPath)
	authorTmpl, err = tmpl.New("author.html").ParseFiles(tmplLinks...)
	if err != nil {
		return err
	}

	var authorData bytes.Buffer
	if err = postTmpl.Execute(&authorData, nil); err != nil {
		return
	}
	authorStatics = utils.GetAllStaticLinks(authorData.Bytes(), glb.StaticRegex)
	authorDataLen = authorData.Len()
	return nil
}

func loadTagTemplate(tagPath string) (err error) {
	tagDataLen = 0

	if tagPath == "" {
		tagPath = glb.DefaultTagTmplPath
	}
	tmp, err := ioutil.ReadFile(tagPath)
	if err != nil {
		return err
	}
	tmplLinks := append(utils.GetAllNestedTemplates(tmp, glb.NestedTmplRegex), tagPath)
	tagTmpl, err = tmpl.New("tag.html").ParseFiles(tmplLinks...)
	if err != nil {
		return err
	}

	var tagData bytes.Buffer
	if err = tagTmpl.Execute(&tagData, nil); err != nil {
		return
	}
	tagStatics = utils.GetAllStaticLinks(tagData.Bytes(), glb.StaticRegex)
	tagDataLen = tagData.Len()
	return nil
}

func loadTemplateData() error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	favIconData, _ = ioutil.ReadFile(filepath.Join(wd, "templates", "static", "img", "fav", "favicon.ico"))

	if loginData, err = ioutil.ReadFile(filepath.Join(wd, "templates", "login.html")); err != nil {
		return err
	}
	loginStatics = utils.GetAllStaticLinks(loginData, glb.StaticRegex)
	loginDataLen = len(loginData)

	if adminData, err = ioutil.ReadFile(filepath.Join(wd, "templates", "admin.html")); err != nil {
		return err
	}
	adminStatics = utils.GetAllStaticLinks(adminData, glb.AStaticRegex)
	adminDataLen = len(adminData)

	if notFoundData, err = ioutil.ReadFile(filepath.Join(wd, "templates", "404.html")); err != nil {
		return err
	}
	notFoundStatics = utils.GetAllStaticLinks(notFoundData, glb.StaticRegex)

	return nil
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	w.Write(notFoundData)
	p, ok := w.(http.Pusher) // HTTP/2
	if ok {
		for _, v := range notFoundStatics {
			log.Debugf("HTTP2 pushed: %v", v)
			p.Push(v, nil)
		}
	}
}
