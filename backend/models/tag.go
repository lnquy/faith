package models

import (
	"time"
)

type Tag struct {
	//gorm.Model
	UserID    uint      `json:"user_id" gorm:"primary_key"`
	CreatedAt time.Time `json:"created_at"`

	Name    string `json:"tag_name" gorm:"type:varchar(255);not null;unique"`
	PostIDs uint   `json:"post_ids" gorm:"column:post_ids"`
}
