package models

import (
	"errors"
	"fmt"
	cfg "github.com/lnquy/faith/backend/conf"
	"github.com/lnquy/faith/backend/utils"
	"io/ioutil"
	"os"
	"time"
)

type MediaMetadata struct {
	//gorm.Model
	MediaID   uint      `json:"media_id" gorm:"primary_key"`
	CreatedAt time.Time `json:"created_at"`

	OwnerID     uint   `json:"owner_id" gorm:"type:integer; not null"`
	Filename    string `json:"filename" gorm:"type:varchar(500);not null"`
	Title       string `json:"title" gorm:"type:varchar(500)"`
	Description string `json:"description" gorm:"type:varchar(1500)"`
	Type        string `json:"type" gorm:"type:varchar(30);not null"`
	Size        uint   `json:"size" gorm:"type:integer;not null"`
	StoragePath string `json:"path" gorm:"type:varchar(500);not null"`
	WebPath     string `json:"web_path" gorm:"type:varchar(500);not null"`
	//SamplePath  string `json:"sample_path" gorm:"type:varchar(500);not null"` // TODO: Media sampling
}

// RawMedia is a wrapper struct to map with upload form from frontend
type RawMedia struct {
	MediaMetadata
	Data string `json:"data"`
}

func (r *RawMedia) Validate() (err error) {
	if r.Filename == "" || r.Type == "" || r.Size == 0 {
		return errors.New("Invalid media. File name, type and size must be specified")
	}
	// TODO: Limit max media size by its type
	if len(r.Data) == 0 || len(r.Data) > cfg.Global.Media.MaxSize {
		return errors.New(fmt.Sprintf("Invalid media. Size must != 0 and < %.2fMB", cfg.Global.Media.MaxSize/1000000))
	}
	if r.Title == "" {
		r.Title = r.Filename
	}
	r.Filename = utils.GetUniqueFilename(r.Filename) // Override file name by unique timestamp filename
	return nil
}

func (m *MediaMetadata) Upload(raw []byte) (err error) {
	if err = ioutil.WriteFile(m.StoragePath, raw, 0666); err != nil {
		return
	}

	if err = faithDb.Create(m).Error; err != nil {
		os.Remove(m.StoragePath)
		return
	}
	return nil
}

func (m *MediaMetadata) GetAbsolutePath() {
	faithDb.Model(&MediaMetadata{}).Select("storage_path").Where("media_id = ?", m.MediaID).Scan(m)
	return
}

func (m *MediaMetadata) Delete() (err error) {
	m.GetAbsolutePath()
	if err = os.Remove(m.StoragePath); err != nil {
		return
	}

	err = faithDb.Model(&MediaMetadata{}).Delete(m).Error
	return
}

func DeleteBatchMedia(midList []string) (err error) {
	// TODO: Only allow to delete media belong to user
	var media []MediaMetadata
	err = faithDb.Model(&MediaMetadata{}).Where("media_id in (?)", midList).Scan(&media).Error
	if err != nil {
		return
	}
	for _, m := range media {
		err = m.Delete()
		if err != nil {
			return
		}
	}

	return nil
}

func (m *MediaMetadata) CheckMediaOwner() bool {
	var r int
	faithDb.Model(&MediaMetadata{}).Where("media_id = ? and owner_id = ?", m.MediaID, m.OwnerID).Count(&r)
	return r == 1
}

func GetAllMedia() (media []MediaMetadata, err error) {
	err = faithDb.Model(&MediaMetadata{}).Scan(&media).Error
	return
}
