package models

import "time"

type Author struct {
	//gorm.Model
	AuthorID  uint      `json:"author_id" gorm:"primary_key"`
	CreatedAt time.Time `json:"created_at"`

	UserID uint // Foreign key to User table

	Link   string `json:"link"`
	Posts  int    `json:"posts_number"`
}

func GetAuthor(uid uint) (a Author, err error) {
	u := &User{
		UserID: uid,
	}
	err = faithDb.Model(u).Related(&a).Error
	return
}
