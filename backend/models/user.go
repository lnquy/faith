package models

import (
	"errors"
	"golang.org/x/crypto/bcrypt"
	"regexp"
	"time"
)

type User struct {
	//gorm.Model
	UserID    uint      `json:"user_id" gorm:"primary_key"`
	CreatedAt time.Time `json:"created_at"`

	Author Author // One-to-one with Author table

	UserAccount
	UserInfo
	Authority
}

type UserAccount struct {
	Username string `json:"username" gorm:"type:varchar(21);not null;unique"`
	Email    string `json:"email" gorm:"type:varchar(75);not null;unique"`
	Password string `json:"-" gorm:"type:varchar(65)"`
}

type Avatar struct {
	ID  uint   `json:"avatar_id" gorm:"column:avatar_id"`
	Src string `json:"avatar_src" gorm:"column:avatar_src;type:varchar(255)"`
}

type UserInfo struct {
	Avatar
	FirstName      string `json:"first_name" gorm:"type:varchar(30)"`
	LastName       string `json:"last_name" gorm:"type:varchar(60)"`
	DisplayNameOpt int    `json:"display_name_opt"`
	DisplayName    string `json:"display_name" gorm:"type:varchar(90)"`
	Birthday       string `json:"birthday" gorm:"type:datetime"`
	Phone          string `json:"phone" gorm:"type:varchar(20)"`
	Address        string `json:"address" gorm:"type:varchar(500)"`
	PostalCode     string `json:"postal_code" gorm:"type:varchar(11)"`
	IntroduceText  string `json:"introduce_text" gorm:"varchar(500)"`
}

var usernameRegx = regexp.MustCompile(`^[a-zA-Z0-9_\-]{4,20}$`)
var emailRegx = regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)

func (uc *UserAccount) ValidateRegisterData() error {
	if err := uc.ValidateLoginData(); err != nil {
		return err
	}
	if !emailRegx.MatchString(uc.Email) || len(uc.Email) > 75 {
		return errors.New("Invalid email")
	}
	return nil
}

func (uc *UserAccount) ValidateLoginData() error {
	if !usernameRegx.MatchString(uc.Username) {
		return errors.New("Invalid username")
	}
	if len(uc.Password) < 8 || len(uc.Password) > 30 {
		return errors.New("Invalid password: Length from 8 to 30 characters")
	}
	return nil
}

func (uc *UserAccount) Create() (err error) {
	var bHash []byte
	if bHash, err = bcrypt.GenerateFromPassword([]byte(uc.Password), bcrypt.DefaultCost); err != nil {
		return err
	}

	uc.Password = string(bHash)
	user := &User{
		UserAccount: *uc,
		Authority:   *InitUserAuthority(NormUser),
	}
	if err = faithDb.Model(&User{}).Create(user).Error; err != nil {
		return
	}
	return nil
}

func (uc *UserAccount) Login() (user User, err error) {
	if err = faithDb.Model(&User{}).Where("username = ?", uc.Username).First(&user).Error; err != nil {
		return User{}, errors.New("Account not found. Please check your username!")
	}
	//var bHash []byte
	//if bHash, err = bcrypt.GenerateFromPassword([]byte(uc.Password), bcrypt.DefaultCost); err != nil {
	//	return User{}, err
	//}
	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(uc.Password)); err != nil {
		return User{}, errors.New("Password not matched. Please check your password!")
	}
	return
}

func (u *User) GetDisplayName() (name string) {
	if u.FirstName != "" {
		name = u.FirstName
	}
	if u.LastName != "" {
		name = name + " " + u.LastName
	}
	if name == "" {
		name = u.Username
	}
	return
}

func CountUsers() (c int) {
	faithDb.Model(&User{}).Count(&c)
	return
}

func GetUserByUID(uid uint) (user User, err error) {
	err = faithDb.Model(&User{}).First(&user, uid).Error
	return
}

func UpdateLastActive(uid uint) error {
	return faithDb.Model(&User{}).Where("user_id = ?", uid).Update("last_active", time.Now().UTC().String()).Error
}

func (u *User) UpdatePassword() error {
	return faithDb.Model(u).Updates(map[string]interface{}{
		"password": u.Password,
	}).Error
}

func (u *User) UpdateProfile() error {
	return faithDb.Model(u).Updates(map[string]interface{}{
		"email":            u.Email,
		"first_name":       u.FirstName,
		"last_name":        u.LastName,
		"display_name_opt": u.DisplayNameOpt,
		"display_name":     u.DisplayName,
	}).Error
}

func (u *User) UpdateAvatar() error {
	return faithDb.Model(u).Updates(map[string]interface{}{
		"avatar_id":  u.Avatar.ID,
		"avatar_src": u.Avatar.Src,
	}).Error
}

func (u *User) UpdateInfo() error {
	return faithDb.Model(u).Updates(map[string]interface{}{
		"birthday":       u.Birthday,
		"phone":          u.Phone,
		"address":        u.Address,
		"postal_code":    u.PostalCode,
		"introduce_text": u.IntroduceText,
	}).Error
}

func (u *User) UpdateAuthority() error {
	return faithDb.Model(u).Updates(map[string]interface{}{
		"is_active":    u.IsActive,
		"last_active":  u.LastActive,
		"account_type": u.AccountType,
	}).Error
}

func DeleteUser(uid uint) error {
	return faithDb.Model(&User{}).Where("user_id = ?", uid).Delete(User{}).Error
}

func InitAdminAccount() (err error) {
	var bHash []byte
	if bHash, err = bcrypt.GenerateFromPassword([]byte("12345678"), bcrypt.DefaultCost); err != nil {
		return
	}
	user := &User{
		UserAccount: UserAccount{
			Username: "admin",
			Password: string(bHash),
			Email:    "admin@lnquy.com",
		},
		UserInfo: UserInfo{
			Avatar: Avatar{
				ID:  0,
				Src: "../public/img/default_avatar.png",
			},
			DisplayNameOpt: 0,
			DisplayName:    "admin",
		},
		Authority: *InitUserAuthority(Admin),
	}
	err = faithDb.Model(&User{}).Create(user).Error
	return
}

func CheckPassword(uid uint, pw string) (user User, err error) {
	if err = faithDb.Model(&User{}).Where("user_id = ?", uid).First(&user).Error; err != nil {
		return User{}, errors.New("Account not found. Please check your username!")
	}
	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(pw)); err != nil {
		return User{}, errors.New("Password not matched. Please check your password!")
	}
	return
}
