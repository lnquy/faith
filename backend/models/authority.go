package models

import (
	"time"
)

type Authority struct {
	AccountType byte   `json:"account_type" gorm:"not null"`
	LastActive  string `json:"last_active"  gorm:"type:datetime; not null"`
	IsActive    bool   `json:"is_active" gorm:"not null"`
}

type AccountType byte

const (
	Admin AccountType = iota
	Moder
	AdvUser
	NormUser
	Anon
)

func InitUserAuthority(ac AccountType) *Authority {
	return &Authority{
		AccountType: byte(ac),
		LastActive:  time.Now().UTC().String(),
		IsActive:    true,
	}
}
