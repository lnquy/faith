package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/lnquy/faith/backend/utils"
	cfg "github.com/lnquy/faith/backend/conf"
	"log"
	"path"
	glb "github.com/lnquy/faith/backend/modules/global"
)

var faithDb *gorm.DB // Singleton

// MigrateDatabases automatically do migration for faith's databases
// based on the models
// TODO: Let admin choose to init database later
func MigrateDatabases(dbPath string) (err error) {
	if err := utils.EnsurePath(dbPath); err != nil {
		log.Fatalf("Cannot create directory to %s! %s", dbPath, err)
	}

	dbPath = path.Join(dbPath, "faith.db")
	faithDb, err = gorm.Open("sqlite3", dbPath)
	if err != nil {
		log.Fatalf("Failed to connect database: %s (%s)", err, dbPath)
	}
	faithDb.AutoMigrate(&User{}, &Author{}, &Post{}, &Draft{}, &MediaMetadata{})

	// Init admin account if server first run
	if CountUsers() == 0 {
		if err := InitAdminAccount(); err != nil {
			log.Fatalf("Can not create administrator account. Err: %s", err)
		} else {
			log.Print("Administrator account has been created.")
		}
	}

	if cfg.Global.Server.RunMode == glb.Development {
		faithDb.LogMode(true)
	}
	return nil
}
