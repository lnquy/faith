package middleware

import (
	"gopkg.in/kataras/iris.v6"
	"net/http"
	"github.com/lnquy/faith/backend/conf"
)

func SessionMiddleware(ctx *iris.Context) {
	if conf.IS_DEVELOPMENT {
		ctx.Next()
	} else {
		if uid := ctx.Session().Get("uid"); uid == nil {
			ctx.JSON(http.StatusUnauthorized, "Unauthorized. Please login first!")
			return
		}
		ctx.Next()
	}
}

func AdminMiddleware(ctx *iris.Context) {
	if conf.IS_DEVELOPMENT {
		ctx.Next()
	} else {
		if uid := ctx.Session().Get("uid"); uid == nil {
			ctx.Redirect("/login", http.StatusFound) // Requires login first
			return
		}
		ctx.Next()
	}
}
