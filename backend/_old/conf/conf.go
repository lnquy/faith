package conf

const (
	DefaultOffset = 0
)

var (
	// Server
	IS_DEVELOPMENT = true
	ENABLE_GZIP    = true

	// Frontend request
	GET_POST_SIZE = 50

	// Media
	MAX_MEDIA_SIZE = 100000000 // Bytes (100MB)

	// Post
	DEFAULT_PROTECTED_POST_PWD = "abhgtmsd@0406$" // A broken-hearted-guy trying to make something different =))
)
