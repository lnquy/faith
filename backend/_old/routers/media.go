package routers

import (
	"fmt"
	"github.com/lnquy/faith/backend/models"
	u "github.com/lnquy/faith/backend/utils"
	"github.com/vincent-petithory/dataurl"
	"gopkg.in/kataras/iris.v6"
	"net/http"
	"path"
	"strings"
)

func UploadMedia(ctx *iris.Context) {
	var raw models.RawMedia
	if err := ctx.ReadJSON(&raw); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}

	if err := raw.Validate(); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}

	uid := u.GetAccessUId(ctx)
	raw.OwnerID = uid
	sPath := path.Join("data", "users", fmt.Sprintf("%d", uid), raw.Type)
	if err := u.EnsurePath(sPath); err != nil {
		ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
		return
	}
	sPath = path.Join(sPath, raw.Filename)
	raw.StoragePath = sPath
	raw.WebPath = path.Join("..", sPath) // Relative path, root is templates folder
	if data, err := dataurl.DecodeString(raw.Data); err != nil {
		ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
	} else {
		// TODO: Thumbnail image
		if err := raw.MediaMetadata.Upload(data.Data); err != nil {
			ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
		} else {
			ctx.JSON(http.StatusOK, raw.MediaMetadata)
		}
	}
}

func GetAllMedia(ctx *iris.Context) {
	// TODO: Get media by userID
	if media, err := models.GetAllMedia(); err != nil {
		ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
	} else {
		ctx.JSON(http.StatusOK, media)
	}
}

func DeleteMedia(ctx *iris.Context) {
	midList := strings.Split(ctx.Param("mid"), ",")

	if err := models.DeleteBatchMedia(midList); err != nil {
		ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
	} else {
		ctx.JSON(http.StatusOK, "OK")
	}
}
