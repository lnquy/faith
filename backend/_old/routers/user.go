package routers

import (
	"fmt"
	"github.com/lnquy/faith/backend/models"
	u "github.com/lnquy/faith/backend/utils"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/kataras/iris.v6"
	"log"
	"net/http"
	"strings"
	"time"
)

type LoginForm struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Login verify user login and return new token and session if login successfully
// Post login
func Login(ctx *iris.Context) {
	// TODO: Remember me
	var form LoginForm
	if err := ctx.ReadJSON(&form); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}

	uc := models.UserAccount{
		Username: form.Username,
		Password: form.Password,
	}
	// TODO
	if err := uc.ValidateLoginData(); err != nil {
		log.Printf("Invalid login data! %s", err)
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}

	if user, err := uc.Login(); err != nil {
		log.Printf("Login failed: %s", err)
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
	} else {
		// TODO: Update last_active field
		log.Printf("User logged in: %v", user.Username)
		// Sessions
		ctx.Session().Set("uid", user.UserID)
		ctx.Session().Set("username", user.Username)
		ctx.Session().Set("authority", user.Authority)
		ctx.SetCookie(&http.Cookie{
			Name:    "uid",
			Value:   fmt.Sprintf("%d|%s|%s", user.UserID, user.Username, user.GetDisplayName()),
			Expires: time.Now().AddDate(0, 1, 0), // TODO: config
		})
		ctx.JSON(http.StatusOK, "OK") // Redirect to admin page on front end
	}
}

func GetFullUserInfo(ctx *iris.Context) {
	if uid, err := ctx.ParamInt("uid"); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
	} else {
		if user, err := models.GetUserByUID(uint(uid)); err != nil {
			ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
		} else {
			ctx.JSON(http.StatusOK, user)
		}
	}
}

type PasswordForm struct {
	CurPass string `json:"cur_pass"`
	NewPass string `json:"new_pass"`
}

func ChangePassword(ctx *iris.Context) {
	var pwForm PasswordForm
	if err := ctx.ReadJSON(&pwForm); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}
	//TODO: Validate form
	log.Println(pwForm)
	uid := u.GetAccessUId(ctx)

	if user, err := models.CheckPassword(uid, pwForm.CurPass); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
	} else {
		var bHash []byte
		if bHash, err = bcrypt.GenerateFromPassword([]byte(pwForm.NewPass), bcrypt.DefaultCost); err != nil {
			ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
			return
		}

		user.Password = string(bHash[:])
		if err := user.UpdatePassword(); err != nil {
			ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
		} else {
			ctx.JSON(http.StatusOK, "OK")
		}
	}
}

func Logout(ctx *iris.Context) {
	ctx.SessionDestroy()
	ctx.JSON(http.StatusOK, "OK")
}

func UpdateUser(ctx *iris.Context) {
	var uid int
	var err error
	if uid, err = ctx.ParamInt("uid"); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}

	var user models.User
	if err = ctx.ReadJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}

	user.UserID = uint(uid)
	uType := ctx.Request.URL.String()
	if strings.Contains(uType, "profile") {
		err = user.UpdateProfile()
	} else if strings.Contains(uType, "info") {
		err = user.UpdateInfo()
	} else if strings.Contains(uType, "avatar") {
		err = user.UpdateAvatar()
	} else if strings.Contains(uType, "authority") {
		err = user.UpdateAuthority()
	}

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
	} else {
		ctx.JSON(http.StatusOK, "OK")
	}
}
