package routers

import (
	"gopkg.in/kataras/iris.v6"
	"log"
	"net/http"
	"github.com/lnquy/faith/backend/models"
	u "github.com/lnquy/faith/backend/utils"
	"github.com/lnquy/faith/backend/conf"
	"html/template"
)

// TODO: Need a map link model to map between postID URL and date/name URL

func PostPage(ctx *iris.Context) {
	if pid, err := ctx.ParamInt("pid"); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
	} else {
		if post, err := models.GetPost(uint(pid)); err != nil {
			ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()}) // TODO: 404 Not found post
		} else {
			publishDate := post.PublishDate.Format("Jan 2 2006")
			if post.ImgSrc == "" {
				post.ImgSrc = "../public/img/about-bg.jpg"
			}
			data := struct {
				Title       string
				Post        models.Post
				RawContent  template.HTML
				PublishDate string
			}{
				Title:       post.Title,
				Post:        post,
				RawContent:  template.HTML(post.Content),
				PublishDate: publishDate,
			}
			if err := ctx.Render("post.html", data); err != nil {
				log.Printf("Parse template error: %s", err)
			}
		}
	}
}

func CreatePost(ctx *iris.Context) {
	var p models.Post
	if err := ctx.ReadJSON(&p); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}

	if err := p.Validate(); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
		return
	}

	uid := u.GetAccessUId(ctx)
	p.OwnerID = uid

	if p.PostID == 0 { // Post never be saved, save to Posts table first
		if err := p.Create(); err != nil {
			ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
			return
		}
		if p.Draft { // If it's draft post then save to Drafts table, too
			if d, err := models.CreateDraftFrom(&p); err != nil {
				ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
				return
			} else {
				ctx.JSON(http.StatusOK, d)
				return
			}
		}
	} else { // Post already saved on Posts table
		if p.Draft { // Update post in Posts to the latest version of draft if its state is drafting
			if _, err := models.CreateDraftFrom(&p); err != nil {
				ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
				return
			} else {
				if err := p.UpdateLatestDraft(); err != nil {
					ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
					return
				}
			}
		} else { // Update current post in Posts
			if err := p.Update(); err != nil {
				ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
				return
			}
		}
	}

	ctx.JSON(http.StatusOK, p)
}

func GetPostsList(ctx *iris.Context) {
	var offset int
	var err error
	if offset, err = ctx.ParamInt("offset"); err != nil {
		offset = conf.DefaultOffset
	}

	if posts, err := models.GetPostsList(conf.GET_POST_SIZE, offset); err != nil {
		ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
	} else {
		ctx.JSON(http.StatusOK, posts)
	}
}

func GetPost(ctx *iris.Context) {
	if pid, err := ctx.ParamInt("pid"); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
	} else {
		if post, err := models.GetPost(uint(pid)); err != nil {
			ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
		} else {
			ctx.JSON(http.StatusOK, post)
		}
	}
}

func DeletePost(ctx *iris.Context) {
	if pid, err := ctx.ParamInt("pid"); err != nil {
		ctx.JSON(http.StatusBadRequest, u.Err{err.Error()})
	} else {
		uid := u.GetAccessUId(ctx)
		p := models.Post{
			PostID: uint(pid),
			PostMeta: models.PostMeta{
				OwnerID: uid,
			},
		}
		if !p.CheckPostOwner() {
			ctx.JSON(http.StatusForbidden, &u.Err{"Permission requires to delete this post"})
			return
		}

		if err := p.Delete(); err != nil {
			ctx.JSON(http.StatusInternalServerError, u.Err{err.Error()})
		} else {
			ctx.JSON(http.StatusOK, "OK")
		}
	}
}
