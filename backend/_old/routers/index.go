package routers

import (
	"gopkg.in/kataras/iris.v6"
	"log"
)

func IndexPage(ctx *iris.Context) {
	data := &struct{ Title string }{Title: "Faith Blog"}
	if err := ctx.Render("index.html", data); err != nil {
		log.Printf("Parse template error: %s", err)
	}
}
