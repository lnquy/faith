package routers

import (
	"gopkg.in/kataras/iris.v6"
	"log"
)

func AdminPage(ctx *iris.Context) {
	if err := ctx.Render("admin.html", nil); err != nil {
		log.Printf("Parse admin template error: %s", err)
	}
}
