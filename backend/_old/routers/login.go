package routers

import (
	"gopkg.in/kataras/iris.v6"
	"log"
	"net/http"
)

// Get login
func LoginPage(ctx *iris.Context) {
	if name := ctx.Session().GetString("username"); name == "" {
		if err := ctx.Render("login.html", nil); err != nil {
			log.Printf("Parse template error: %s", err)
		}
	} else {
		// Redirect to admin page
		ctx.Redirect("/admin", http.StatusFound)
	}
}
