package utils

import (
	"gopkg.in/kataras/iris.v6"
	"github.com/lnquy/faith/backend/conf"
)

func getDevUid(ctx *iris.Context) uint {
	if uid := ctx.Session().Get("uid"); uid == nil {
		return uint(1) // Admin uid
	} else {
		return uid.(uint)
	}
}

// Must make sure uid is always available in production mode
func GetAccessUId(ctx *iris.Context) uint {
	if conf.IS_DEVELOPMENT {
		return getDevUid(ctx)
	} else {
		return (ctx.Session().Get("uid")).(uint)
	}
}
