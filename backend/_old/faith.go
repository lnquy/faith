/************************************
 * Faith - My personal Go blog      *
 * -------------------------------- *
 * @author:  lnquy                  *
 * @email:   lnquy.it@gmail.com     *
 * @date:    April 05th 2017        *
 * @license: MIT License            *
 ************************************/

package main

import (
	"flag"
	"fmt"
	"github.com/lnquy/faith/backend/conf"
	"github.com/lnquy/faith/backend/models"
	mdw "github.com/lnquy/faith/backend/modules/middlewares"
	r "github.com/lnquy/faith/backend/routers"
	"gopkg.in/kataras/iris.v6"
	"gopkg.in/kataras/iris.v6/adaptors/httprouter"
	"gopkg.in/kataras/iris.v6/adaptors/sessions"
	"gopkg.in/kataras/iris.v6/adaptors/view"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
	"gopkg.in/kataras/iris.v6/adaptors/cors"
)

var (
	serverIP   = "127.0.0.1"
	serverPort = 80 // TODO: HTTPS

	// Flags
	fAddr *string
)

func init() {
	fAddr = flag.String("a", "127.0.0.1:80", "Server address (Ex: 10.10.1.123, 192.168.1.100:8080...)")
}

func main() {
	// TODO: Configuration
	cmdConfig()

	// Database initialization
	dbPath, err := os.Getwd()
	if err != nil {
		log.Panicf("Cannot detect current working directory! %s", err)
	}
	dbPath = path.Join(dbPath, "data", "db")
	log.Printf("DB path: %s", dbPath)
	if err := models.MigrateDatabases(dbPath); err != nil {
		log.Panicf("Failed to migrate databases! %s", err) // TODO: Allow admin to configure via web GUI
	}
	//defer models.faithDb.Close() // TODO: IMPORTANT!!!

	// Iris server
	faith := iris.New(iris.Configuration{Gzip: conf.ENABLE_GZIP})
	configServer(faith)

	// TODO
	//if serverIP, err = utils.GetInternalIP(); err != nil {
	//	serverIP = "127.0.0.1"
	//}

	//if conf.IS_DEVELOPMENT {
	faith.Listen(fmt.Sprintf("%s:%d", serverIP, serverPort))
	//} else {
	//	faith.ListenLETSENCRYPT(fmt.Sprintf("%s:%d", serverIP, serverPort))
	//}
}

func cmdConfig() {
	flag.Parse()
	if strings.Contains(*fAddr, ":") {
		idx := strings.Index(*fAddr, ":")
		serverIP = (*fAddr)[0:idx]
		serverPort, _ = strconv.Atoi((*fAddr)[idx+1: len(*fAddr)])
	} else {
		serverIP = *fAddr
	}
}

func configServer(faith *iris.Framework) {
	// Sessions
	fSession := sessions.New(sessions.Config{
		Cookie: "fid", // Faith session ID
		//DecodeCookie:                false,
		Expires:                     time.Duration(168) * time.Hour, // 7 days
		CookieLength:                32,
		DisableSubdomainPersistence: false,
	})

	// Adaptors
	faith.Adapt(
		iris.DevLogger(),
		httprouter.New(),
		view.HTML("./templates", ".html").Reload(conf.IS_DEVELOPMENT),
		fSession,
		// TODO: Allow CORS for local dev only
		cors.New(cors.Options{
			AllowedOrigins: []string{"*"},
			AllowedMethods: []string{"POST", "GET", "DELETE", "PUT", "OPTIONS", "HEAD"},
			AllowedHeaders: []string{"*"},
		}),
	)

	// Routing
	// TODO: Custom handler for 404
	faith.OnError(iris.StatusNotFound, func(ctx *iris.Context) {
		ctx.HTML(iris.StatusNotFound, "<h1>Custom not found handler </h1>")
	})
	faith.Favicon("./public/img/fav")
	faith.StaticWeb("/public", "./public")             // Static files
	faith.StaticWeb("/istatic", "./templates/istatic") // Index static files
	faith.StaticWeb("/astatic", "./templates/astatic") // Admin static files
	faith.StaticWeb("/data", "./data")                 // User's media
	faith.Get("/", r.IndexPage)
	faith.Get("/post/:pid", r.PostPage)
	faith.Get("/login", r.LoginPage)
	faith.Post("/login", r.Login)

	faith.Get("/admin", mdw.AdminMiddleware, r.AdminPage)
	api := faith.Party("/api", mdw.SessionMiddleware)
	{
		// Media
		api.Post("/media", r.UploadMedia)
		api.Get("/media", r.GetAllMedia)
		api.Delete("/media/:mid", r.DeleteMedia)
		// Post
		api.Post("/post", r.CreatePost)
		api.Delete("/post/:pid", r.DeletePost)
		// Users
		//api.Post("/user") // Register
		//api.Get("/users") // Get all users
		api.Get("/user/:uid", r.GetFullUserInfo)
		api.Post("/user/:uid/avatar") // Change avatar
		api.Put("/user/:uid/pw", r.ChangePassword)
		api.Put("/user/:uid/profile", r.UpdateUser)
		api.Put("/user/:uid/avatar", r.UpdateUser)
		api.Put("/user/:uid/info", r.UpdateUser)
		api.Put("/user/:uid/authority", r.UpdateUser)
		api.Get("/user/:uid/logout", r.Logout)
	}

	faith.Get("/api/posts", r.GetPostsList) // Allow to get posts list in frontpage
	faith.Get("/api/post/:pid", r.GetPost)

	// TODO: Move to api party with auth later

	//faith.Get("/logout", r.Logout)
	//faith.Post("/api/users/new", r.Register)
	//faith.Put("/api/users", r.UpdateUser)
	//faith.Delete("/api/users", r.DeleteUser)
	// Media

	//// Posts
	//faith.Post("/api/posts/new", r.NewPost)
	//faith.Get("/api/posts", r.GetPostsByUser)    // TODO: This only return list of summarized posts for timeline rendering
	//faith.Get("/api/posts/:pid", r.GetPostContentByPID) // This will return full post's content only
	//faith.Put("/api/posts/:pid", r.UpdatePost)   //
	//faith.Delete("/api/posts/:pid", r.DeletePost)
	//// Messages
	//faith.Post("/api/messages/new", r.NewMessage)
	//faith.Get("/api/messages", r.GetMessagesByUser)
	//faith.Delete("/api/messages/:mid", r.DeleteMessage)
	//// Others
	//faith.Get("/api/whoami", r.WhoAmI)
	//faith.Post("/api/media/upload", r.UploadMedia)
	////faith.Get("/*", r.NotFound404)
	////faith.Get("/test", r.Test)
	////faith.Get("/secret", jwtHandler.Serve, r.SecretIndexPage) // TODO: Test
}
