package models

import (
	"time"
	"errors"
	u "github.com/lnquy/faith/backend/utils"
)

type Draft struct {
	DraftID   uint      `json:"draft_id" gorm:"primary_key"`
	CreatedAt time.Time `json:"created_at"`

	PostID uint `json:"post_id" gorm:"type:integer;not null"`
	PostMeta
}

func CreateDraftFrom(p *Post) (d *Draft, err error) {
	d = &Draft{
		PostID:   p.PostID,
		PostMeta: p.PostMeta,
	}
	return d, d.Create()
}

func (d *Draft) Create() (err error) {
	if d.CheckDuplicatedPost() {
		return errors.New("Duplicated draft post")
	}
	return faithDb.Model(&Draft{}).Create(d).Error
}

func (d *Draft) CheckDuplicatedPost() (res bool) {
	var ds []Draft
	faithDb.Model(&Draft{}).Select("content").Where("post_id = ? AND title = ?", d.PostID, d.Title).Scan(&ds)
	if len(ds) == 0 {
		return
	}
	for _, v := range ds { // Check content duplication
		if u.Compare(d.Content, v.Content) {
			return true
		}
	}
	return
}

func DeleteDraftsByPostId(pid uint) error {
	return faithDb.Where("post_id = ?", pid).Delete(&Draft{}).Error
}
