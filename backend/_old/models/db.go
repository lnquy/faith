package models

import (
	"github.com/jinzhu/gorm"
	"log"
	_"github.com/jinzhu/gorm/dialects/sqlite"
	"path"
	u "github.com/lnquy/faith/backend/utils"
)

var faithDb *gorm.DB // Singleton

// MigrateDatabases automatically do migration for faith's databases
// based on the models
// TODO: Let admin choose to init database later
func MigrateDatabases(dbPath string) (err error) {
	if err := u.EnsurePath(dbPath); err != nil {
		log.Fatalf("Cannot create directory to %s! %s", dbPath, err)
	}

	dbPath = path.Join(dbPath, "faith.db")
	faithDb, err = gorm.Open("sqlite3", dbPath)
	if err != nil {
		log.Fatalf("Failed to connect database: %s (%s)", err, dbPath)
	}
	faithDb.AutoMigrate(&User{}, &Post{}, &Draft{}, &MediaMetadata{})

	// Init admin account if server first run
	if CountUsers() == 0 {
		if err := InitAdminAccount(); err != nil {
			log.Fatalf("Can not create administrator account. Err: %s", err)
		} else {
			log.Print("Administrator account has been created.")
		}
	}

	return nil
}
