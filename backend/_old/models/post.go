package models

import (
	"errors"
	"time"
	"github.com/lnquy/faith/backend/conf"
	u "github.com/lnquy/faith/backend/utils"
)

type Post struct {
	PostID    uint      `json:"post_id" gorm:"primary_key"`
	CreatedAt time.Time `json:"created_at"`

	PostMeta
	Draft bool `json:"is_draft" gorm:"not null"`
}

type PostMeta struct {
	// TODO: Careful with Date and Password field, may replace by other names [?]
	OwnerID     uint   `json:"owner_id"`
	Title       string `json:"title" gorm:"type:varchar(255);not null"`
	Subtitle    string `json:"subtitle" gorm:"type:varchar(255)"`
	SummaryText string `json:"summary_text" gorm:"type:varchar(500)"`
	HeaderImage `json:"img"`
	Url         string `json:"url" gorm:"type:varchar(255)"`
	Content     string `json:"content" gorm:"type:varchar(100000);not null"`
	PublishDate time.Time `json:"publish_date"`
	Type        string `json:"type" gorm:"type:varchar(50)"`
	Author      string `json:"author" gorm:"type:varchar(255)"`
	Tags        string `json:"tags" gorm:"type:varchar(500)"`
	Protected   bool   `json:"is_protected"`
	Password    string `json:"password" gorm:"type:varchar(65)"`
}

type HeaderImage struct {
	ImgId  uint   `json:"id"`
	ImgSrc string `json:"src"`
}

type PostType byte

const (
	Text  PostType = iota
	Poem
	Image
	Event
)

func (p *Post) Validate() error {
	if p.Title == "" || p.Content == "" {
		return errors.New("Post's title and content must be specified")
	}
	//if p.PublishDate == nil {
	//	p.PublishDate = time.Now().UTC()
	//}
	if p.Protected && p.Password == "" {
		p.Password = conf.DEFAULT_PROTECTED_POST_PWD
	}
	return nil
}

func (p *Post) Create() (err error) {
	if p.CheckDuplicatedPost() {
		return errors.New("Duplicated post")
	}

	return faithDb.Model(&Post{}).Create(p).Error
}

func (p *Post) CheckDuplicatedPost() (res bool) {
	var ps []Post
	faithDb.Model(&Post{}).Select("content").Where("owner_id = ? AND title = ?", p.OwnerID, p.Title).Scan(&ps)
	if len(ps) == 0 {
		return
	}
	for _, v := range ps { // Check content duplication
		if u.Compare(p.Content, v.Content) {
			return true
		}
	}
	return
}

func (p *Post) CheckPostOwner() (res bool) {
	var r int
	faithDb.Model(&Post{}).Where("post_id = ? and owner_id = ?", p.PostID, p.OwnerID).Count(&r)
	return r == 1
}

func (p *Post) Get() (post Post, err error) {
	err = faithDb.Model(&Post{}).Where("post_id = ?", p.PostID).Scan(&post).Error
	return
}

func GetPostsList(limit, offset int) (posts []Post, err error) {
	err = faithDb.Model(&Post{}).
		Select("post_id, created_at, owner_id, title, subtitle, summary_text, img_id, img_src, url, publish_date, type, author, tags, protected, password, draft").
		Order("created_at desc").
		Limit(limit).
		Offset(offset).
		Scan(&posts).
		Error
	return
}

func GetPost(pid uint) (post Post, err error) {
	err = faithDb.Model(&Post{}).Where("post_id = ?", pid).First(&post).Error
	return
}

func (p *Post) GetDraftStatus() (isDraft bool, err error) {
	var post Post
	err = faithDb.Model(&Post{}).Select("draft").Where("post_id = ?", p.PostID).Scan(&post).Error
	return post.Draft, err
}

func (p *Post) UpdateLatestDraft() (err error) {
	var isDraft bool
	if isDraft, err = p.GetDraftStatus(); err != nil {
		return
	}
	if isDraft {
		return p.Update()
	}
	return nil
}

func (p *Post) Update() error {
	return faithDb.Model(&Post{}).Save(p).Error
}

func (p *Post) Delete() (err error) {
	if err = faithDb.Where("post_id = ?", p.PostID).Delete(&Post{}).Error; err != nil {
		return
	}
	return DeleteDraftsByPostId(p.PostID) // Delete all drafts belongs to this post
}

// TODO: Get summarized text only
//func GetPostsByUID(uid uint, limit, offset int) (posts []Post, err error) {
//	err = faithDb.Model(&Post{}).Select("id, owner_id, title, date, type").Where("owner_id = ?", uid).
//		Order("date desc").Limit(limit).Offset(offset).Scan(&posts).Error
//	return
//}
//
//func GetPostsByUIDs(uid, pnid uint, limit, offset int) (posts []Post, err error) {
//	err = faithDb.Model(&Post{}).Select("id, user_id, title, sum_text, date, type").
//		Where("user_id = ?", uid).Or("user_id = ? AND is_private = false", pnid).
//		Order("date desc").Limit(limit).Offset(offset).Scan(&posts).Error
//	return
//}
//
//// TODO: test
//func GetFullPostByPID(pid, uid uint) (post Post, err error) {
//	// TODO
//	err = faithDb.Model(&Post{}).Where("id = ? and user_id = ?", pid, uid).First(&post).Error
//	return
//}
//
//// TODO: test
//func IsPostBelongToUser(pid, uid int) bool {
//	var r int
//	faithDb.Model(&User{}).Where("id = ? and user_id = ?", pid, uid).Count(&r)
//	return r == 1
//}
//
//
