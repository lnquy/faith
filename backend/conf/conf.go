package conf

import (
	"encoding/json"
	glb "github.com/lnquy/faith/backend/modules/global"
	"io/ioutil"
	"log"
	"os"
	"sync"
)

type (
	ServerCfg struct {
		Ip      string `json:"ip"`
		Port    string `json:"port"`
		RunMode string `json:"run_mode"`
		// HTTP over TLS
		EnableHTTPS bool   `json:"enable_https"`
		TLSCert     string `json:"tls_cert"`
		TLSKey      string `json:"tls_key"`
		// Server resource
		ReadTimeout  int `json:"read_timeout_sec"`
		WriteTimeout int `json:"write_timeout_sec"`
		MaxCPUs      int `json:"max_cpus"`
	}
	PerformanceCfg struct {
		// Optimizations
		EnableCompression bool `json:"enable_compression"`
		EnableCache       bool `json:"enable_cache"`
		CacheMaxAge       int  `json:"cache_max_age_sec"`
	}
	PostCfg struct {
		LimitPerPage    int    `json:"limit_per_page"`
		DefaultPassword string `json:"default_protected_password"`
	}
	MediaCfg struct {
		MaxSize int `json:"max_size_byte"`
	}

	// TODO: Move to GUI configuration later
	TempCfg struct {
		PostTags string `json:"post_tags"`
	}

	Config struct {
		Server ServerCfg      `json:"server"`
		Perf   PerformanceCfg `json:"performance"`
		Post   PostCfg        `json:"post"`
		Media  MediaCfg       `json:"media"`
		Temp   TempCfg        `json:"temp"`
	}
)

var (
	rw     sync.RWMutex
	Global *Config // Singleton
)

func init() {
	// Initialize default global configuration
	Global = &Config{
		Server: ServerCfg{
			Ip:           glb.DefaultServerIP,
			Port:         glb.DefaultServerPort,
			RunMode:      glb.Development,
			EnableHTTPS:  true,
			TLSCert:      glb.DefaultTLSCert,
			TLSKey:       glb.DefaultTLSKey,
			ReadTimeout:  glb.DefaultReadTimeout,
			WriteTimeout: glb.DefaultWriteTimeout,
			MaxCPUs:      glb.DefaultMaxCPUs,
		},
		Perf: PerformanceCfg{
			EnableCompression: true,
			EnableCache:       true,
			CacheMaxAge:       glb.DefaultCacheMaxAge,
		},
		Post: PostCfg{
			LimitPerPage:    20,
			DefaultPassword: glb.DefaultPassword,
		},
		Media: MediaCfg{
			MaxSize: 100000000, // Bytes (100MB)
		},
		Temp: TempCfg{}, // TODO
	}
}

func ReadFromFile(cfgFile string) (err error) {
	if cfgFile == "" {
		cfgFile = glb.DefaultConfigFile
	}
	var file *os.File
	if file, err = os.Open(cfgFile); err != nil {
		log.Printf("Open configuration file failed: %s", err)
	} else {
		rw.Lock()
		if err := json.NewDecoder(file).Decode(&Global); err != nil {
			log.Printf("Decode configuration file failed: %s", err)
		}
		rw.Unlock()
	}
	defer file.Close()
	return
}

func WriteToFile(cfgFile string) (err error) {
	if cfgFile == "" {
		cfgFile = glb.DefaultConfigFile // [?] May need to ensure ./config directory
	}
	var faithCfg []byte
	faithCfg, err = json.MarshalIndent(Global, "", "    ")
	return ioutil.WriteFile(cfgFile, faithCfg, 0666)
}
