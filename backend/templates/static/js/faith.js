(function($) {
    "use strict"; // Start of use strict

    // // Floating label headings for the contact form
    // $("body").on("input propertychange", ".floating-label-form-group", function(e) {
    //     $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    // }).on("focus", ".floating-label-form-group", function() {
    //     $(this).addClass("floating-label-form-group-with-focus");
    // }).on("blur", ".floating-label-form-group", function() {
    //     $(this).removeClass("floating-label-form-group-with-focus");
    // });

    // Show the navbar when the page is scrolled up
    let MQL = 1170;

    //primary navigation slide-in effect
    if ($(window).width() > MQL) {
        let headerHeight = $('#mainNav').height();
        $(window).on('scroll', {
                previousTop: 0
            },
            function() {
                let currentTop = $(window).scrollTop();
                //check if user is scrolling up
                if (currentTop < this.previousTop) {
                    //if scrolling up...
                    if (currentTop > 0 && $('#mainNav').hasClass('is-fixed')) {
                        $('#mainNav').addClass('is-visible');
                        $('.navbar-brand svg').addClass('svg-dark');
                    } else {
                        $('#mainNav').removeClass('is-visible is-fixed');
                        $('.navbar-brand svg').removeClass('svg-dark');
                    }
                } else if (currentTop > this.previousTop) {
                    //if scrolling down...
                    $('#mainNav').removeClass('is-visible');
                    if (currentTop > headerHeight && !$('#mainNav').hasClass('is-fixed')) {
                        $('#mainNav').addClass('is-fixed');
                        $('.navbar-brand svg').addClass('svg-dark');
                    }
                }
                this.previousTop = currentTop;
            });
    }

})(jQuery); // End of use strict

/*
 * Replace all SVG images with inline SVG
 */
$(function () {
    jQuery('img.svg').each(function () {
        let $img = jQuery(this);
        let imgID = $img.attr('id');
        let imgClass = $img.attr('class');
        let imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            let $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);
        }, 'xml');

    });
});
