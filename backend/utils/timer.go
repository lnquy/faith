package utils

import (
	"github.com/sirupsen/logrus"
	"time"
)

func ExecTime(name string, start time.Time) {
	logrus.Debugf("Exec time of %s: %v", name, time.Since(start))
}
