package utils

import (
	"github.com/alexedwards/scs/session"
	cfg "github.com/lnquy/faith/backend/conf"
	glb "github.com/lnquy/faith/backend/modules/global"
	"net/http"
)

func getDevUid(r *http.Request) uint {
	if uid, err := session.GetInt(r, "uid"); err != nil || uid == 0 {
		return uint(1) // Admin uid
	} else {
		return uint(uid)
	}
}

// Must make sure uid is always available in production mode
func GetAccessUId(r *http.Request) uint {
	if cfg.Global.Server.RunMode == glb.Development {
		return getDevUid(r)
	} else {
		uid, _ := session.GetInt(r, "uid")
		return uint(uid)
	}
}
