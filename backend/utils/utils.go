package utils

import (
	"errors"
	"fmt"
	cfg "github.com/lnquy/faith/backend/conf"
	glb "github.com/lnquy/faith/backend/modules/global"
	log "github.com/sirupsen/logrus"
	"net"
	"os"
	"regexp"
	"runtime"
	"strings"
	"time"
)

func GetMaxParallelism() int {
	maxProcs := runtime.GOMAXPROCS(0)
	numCPU := runtime.NumCPU()
	if maxProcs < numCPU {
		return maxProcs
	}
	return numCPU
}

func GetInternalIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, iface := range ifaces {
		if (iface.Flags&net.FlagUp == 0) || (iface.Flags&net.FlagLoopback != 0) {
			continue
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return "", err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip != nil {
				return ip.String(), nil
			}
		}
	}
	return "", errors.New("Offline")
}

func EnsurePath(p string) (err error) {
	if _, err = os.Stat(p); os.IsNotExist(err) {
		if err = os.MkdirAll(p, os.ModePerm); err != nil {
			return
		}
	}
	return nil
}

func GetUniqueFilename(name string) string {
	return fmt.Sprintf("%d", time.Now().Unix()) + name[strings.LastIndex(name, "."):]
}

func CheckTLSCertificate() (err error) {
	if _, err = os.Stat(cfg.Global.Server.TLSCert); err != nil {
		return
	}
	if _, err = os.Stat(cfg.Global.Server.TLSKey); err != nil {
		return
	}
	return
}

func GetAllNestedTemplates(data []byte, rgx string) (tmpls []string) {
	var r *regexp.Regexp
	var err error
	if r, err = regexp.Compile(rgx); err != nil {
		log.Error(err)
		return
	}
	matches := r.FindAllStringSubmatch(string(data), -1)
	for _, v := range matches {
		if len(v) == 2 {
			tmpls = append(tmpls, fmt.Sprintf("./templates/%v", v[1]))
		}
	}
	return
}

func GetAllStaticLinks(data []byte, rgx string) []string {
	var r *regexp.Regexp
	var err error
	if r, err = regexp.Compile(rgx); err != nil {
		return []string{}
	}
	return r.FindAllString(string(data), -1)
}

var bannerText = `
 _______  _______  ___   _______  __   __
|       ||   _   ||   | |       ||  | |  |
|    ___||  |_|  ||   | |_     _||  |_|  |
|   |___ |       ||   |   |   |  |       |
|    ___||       ||   |   |   |  |       |
|   |    |   _   ||   |   |   |  |   _   |
|___|    |__| |__||___|   |___|  |__| |__|
------------------------------------------
Faith CMS server %s
Contact: Quy Le (lnquy.it@gmail.com)
------------------------------------------`

func PrintBanner() {
	log.Printf(bannerText, glb.ServerVersion)
}
