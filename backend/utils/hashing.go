package utils

import "github.com/cespare/xxhash"

func Compare(s1, s2 string) bool {
	return xxhash.Sum64String(s1) == xxhash.Sum64String(s2)
}
