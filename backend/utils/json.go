package utils

import (
	"github.com/json-iterator/go"
	"net/http"
)

func DecodeJSON(r *http.Request, data interface{}) error {
	//return json.NewDecoder(r.Body).Decode(data)

	// Note: Careful when using jsoniter since it may ignore some strict validations when
	// marshalling/unmarshalling for performance
	return jsoniter.NewDecoder(r.Body).Decode(data)
}

func EncodeJSON(obj interface{}) (data []byte, err error) {
	return jsoniter.Marshal(obj)
}
