package middlewares

import (
	"net/http"
	"github.com/lnquy/faith/backend/utils"
)

func SessionAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if uid := utils.GetAccessUId(r); uid == 0 {
			http.Error(w, "Authentication required", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}
