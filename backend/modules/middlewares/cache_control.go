package middlewares

import (
	"fmt"
	cfg "github.com/lnquy/faith/backend/conf"
	"net/http"
	"strings"
)

var (
	eTag = `"faith-cache"`
)

func CacheControl(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Etag", eTag)
		w.Header().Set("Cache-Control", fmt.Sprintf("max-age=%d", cfg.Global.Perf.CacheMaxAge))

		if match := r.Header.Get("If-None-Match"); match != "" {
			if strings.Contains(match, eTag) {
				w.WriteHeader(http.StatusNotModified)
				return
			}
		}
		next.ServeHTTP(w, r)
	})
}
