package logger

import (
	"github.com/lnquy/faith/backend/utils"
	log "github.com/sirupsen/logrus"
	"os"
)

var FileLogger *os.File

func GetLogFormatter() *log.TextFormatter {
	return &log.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		FullTimestamp:   true,
	}
}

func InitDevLogger() {
	log.SetLevel(log.DebugLevel)
	log.SetFormatter(GetLogFormatter())
	log.SetOutput(os.Stdout)
}

func InitProdLogger() (err error) {
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(GetLogFormatter())
	utils.EnsurePath("./log")
	if FileLogger, err = os.Create("./log/faith.log"); err != nil {
		return err
	}
	log.SetOutput(FileLogger)
	return
}
