package global

const (
	// Faith
	ServerVersion = "v0.1.0"

	// Config
	DefaultConfigFile = "./config/faith.conf"

	// Server running mode
	Development = "development"
	Production  = "production"
	// Server address
	DefaultServerIP   = "127.0.0.1"
	DefaultServerPort = "443"
	// Server TLS
	DefaultTLSCert = "./config/certs/server.crt"
	DefaultTLSKey  = "./config/certs/server.key"
	// Server resource
	DefaultReadTimeout  = 20
	DefaultWriteTimeout = 20
	DefaultMaxCPUs      = 0 // 0 means use all CPU cores available

	// Performance
	DefaultCacheMaxAge = 604800 // 7 days

	// Posts
	DefaultPassword = "Abhgttmsd@0406$" // A broken-hearted-guy trying to make something different =))

	// Ports
	DefaultHTTPPort  = "80"
	DefaultHTTPSPort = "443"

	DefaultOffset = 0

	// Templates
	DefaultIndexTmplPath  = "./templates/index.html"
	DefaultPostTmplPath   = "./templates/post.html"
	DefaultAuthorTmplPath = "./templates/author.html"
	DefaultTagTmplPath    = "./templates/tag.html"

	// Template regex
	NestedTmplRegex = `{{\s*template\s*"(?P<tmpl>[a-zA-Z0-9.-]+)"\s*\.}}`
	StaticRegex     = `/static/([a-zA-Z0-9./_-]+)`
	AStaticRegex    = `(/astatic/([a-zA-Z0-9./_-]+))|(/static/([a-zA-Z0-9./_-]+))` // TODO
)

var (
	OkMsg = map[string]string{"msg": "Ok"}
)
