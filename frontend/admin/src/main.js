import Vue from 'vue'
import VueResource from 'vue-resource'
import VueChartist from 'vue-chartist'
import VeeValidate from 'vee-validate'

// SemanticUI
// import '../node_modules/semantic-ui-css/semantic.min.css'
import jQuery from 'jquery'
import './assets/semantic.css'
import semantic from 'semantic'
import 'chartist/dist/chartist.css'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

import Admin from './Admin'

import moment from 'moment'
window.moment = moment;

Vue.use(VueResource);
Vue.use(VeeValidate);
Vue.use(VueChartist, {
    messageNoData: "Empty chart",
    classNoData: "empty"
});

Vue.config.productionTip = false;
Vue.http.options.xhr = {withCredentials: true};
Vue.http.options.timeout = 15000;
// Vue.http.options.root = 'https://localhost:8443'; // TODO

// Progress bar
NProgress.configure({
    minimum: 0.15,
    easing: 'ease',
    speed: 500,
    showSpinner: false
});
Vue.http.interceptors.push((request, next) => {
    NProgress.start();
    next((resp) => {
        NProgress.done();
    }, (err) => { // TODO: Is this necessary?
        NProgress.done();
    });
});

export const eventBus = new Vue({
    methods: {
        // Main message
        messageShowed(msg) {
            this.$emit('showMessage', msg);
        },
        loaderShowed(id) {
            this.$emit('showLoader', id);
        },
        loaderHid(id) {
            this.$emit('hideLoader', id);
        },
        // Main sidebar and top menu
        sidebarToggled(sidebar) {
            this.$emit('toggleSidebar', sidebar);
        },
        userLoggedOut() {
            this.$emit('logOutUser');
        },
        // New post page and new post sidebar
        mediaChooserModalShowed() {
            this.$emit('showMediaChooserModal');
        },
        postDeleteModalShowed() {
            this.$emit('showPostDeleteModal');
        },
        postDeleted() {
            this.$emit('deletePostSuccess');
        },
        newPostSidebarInitialized() {
            this.$emit('initNewPostSidebar');
        },
        postsToNewPostSynced(post) {
            this.$emit('syncPostsToNewPost', post);
        },
        newPostContentUpdated(content) {
            this.$emit('updateNewPostContent', content);
        },
        // Posts page
        postMetaModalShowed() {
            this.$emit('showPostMetaModal');
        },
        chartReRendered() {
            this.$emit('reRenderChart');
        },
        postsToNewPostTriggered() {
            this.$emit('triggerPostsToNewPost');
        },
        // Media page
        mediaDetailShowed(media) {
            this.$emit('showMediaDetail', media);
        },
        allMediaSelected(selected) {
            this.$emit('selectAllMedia', selected);
        },
        allMediaCleared() {
            this.$emit('clearAllSelectedMedia');
        },
        mediaModalShowed(mode) {
            this.$emit('showMediaModal', mode);
        },
        mediaInserted(mediaList) {
            this.$emit('insertMedia', mediaList);
        },
    }
});

export const global = {
    getUserId() {
        let uid = this.getCookie('uid');
        // TODO: Change default uid value. 1 for dev only
        return (uid === undefined || uid === "") ? 1 : uid.split('|')[0];
    },
    getUsername() {
        let uid = this.getCookie('uid');
        return (uid === undefined || uid === "") ? "" : uid.split('|')[1];
    },
    getDisplayName() {
        let uid = this.getCookie('uid');
        return (uid === undefined || uid === "") ? "" : uid.split('|')[2];
    },
    getCookie(name) {
        let value = "; " + document.cookie;
        let parts = value.split("; " + name + "=");
        if (parts.length == 2) {
            return parts.pop().split(";").shift();
        }
    },
    cvISO8601ToFaithDate(date) {
        return moment(date, moment.ISO_8601).format('MMM DD YYYY');
    },
    cvFaithDateToISO8601(date) {
        return moment(date, 'MMM DD YYYY').toISOString();
    },
    cvISO8601ToFaithDateTime(date) {
        return moment(date, moment.ISO_8601).format('MMM DD YYYY @ HH:mm');
    },
    cvFaithDateTimeToISO8601(date) {
        return moment(date, 'MMM DD YYYY @ HH:mm').toISOString();
    },
    cvISO8601ToUSDate(date) {
        return moment(date, moment.ISO_8601).format('MM/DD/YYYY');
    },
    cvUSDateToISO8601(date) {
        return moment(date, 'MM/DD/YYYY').toISOString();
    },
};

import {store} from './store/store.js';
new Vue({
    el: '#app',
    store, // Global Vuex instance
    // router,
    template: '<Admin/>',
    components: {Admin}
});
