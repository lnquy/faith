import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import curPageIdx from './modules/cur_page_idx';
import menuTitle from './modules/menu_title';
import newPostEditor from './modules/new_post_editor';
import activePost from './modules/active_post';
import mediaSelected from './modules/media_selected';

export const store = new Vuex.Store({
    modules: {
        curPageIdx,
        menuTitle,
        newPostEditor,
        activePost,
        mediaSelected
    }
});
