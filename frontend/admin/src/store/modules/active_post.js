/**
 * Created by rain on 05/05/17.
 */
const state = {
    activePostId: 0,
    activePostTitle: '',
};

const getters = {
    activePostId: (state) => {
        return state.activePostId;
    },
    activePostTitle: (state) => {
        return state.activePostTitle;
    }
};

const mutations = {
    setActivePostId: (state, payload) => {
        state.activePostId = payload;
    },
    setActivePostTitle: (state, payload) => {
        state.activePostTitle = payload;
    }
};

export default {
    state,
    getters,
    mutations
}


