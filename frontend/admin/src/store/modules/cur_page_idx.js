/**
 * Created by rain on 05/05/17.
 */
const state = {
    curPageIdx: 1 // Dashboard page
};

const getters = {
    curPageIdx: (state) => {
        return state.curPageIdx;
    }
};

const mutations = {
    setCurPageIdx: (state, payload) => {
        state.curPageIdx = payload;
    }
};

export default {
    state,
    getters,
    mutations
}


