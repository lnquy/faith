/**
 * Created by rain on 05/05/17.
 */
const state = {
    menuTitle: 'Dashboard'
};

const getters = {
    menuTitle: (state) => {
        return state.menuTitle;
    }
};

const mutations = {
    setMenuTitle: (state, payload) => {
        state.menuTitle = payload;
    }
};

export default {
    state,
    getters,
    mutations
}


