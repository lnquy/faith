/**
 * Created by rain on 05/05/17.
 */
const state = {
    selectedMedia: [],
};

const getters = {
    selectedMedia: (state) => {
        return state.selectedMedia;
    },
};

const mutations = {
    addSelectedMedia: (state, payload) => {
        state.selectedMedia.push(payload);
    },
    removeSelectedMedia: (state, payload) => {
        let idx = -1;
        for (let i = 0; i < state.selectedMedia.length; i++) {
            if (state.selectedMedia[i].id === payload) {
                idx = i;
                break;
            }
        }
        if (idx !== -1) {
            state.selectedMedia.splice(idx, 1);
        }
    }
};

export default {
    state,
    getters,
    mutations
}


