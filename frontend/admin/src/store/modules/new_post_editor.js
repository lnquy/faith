/**
 * Created by rain on 05/05/17.
 */
const state = {
    editorContent: '',
    summaryText: '',
    contentChanged: false
};

const getters = {
    editorContent: (state) => {
        return state.editorContent;
    },
    summaryText: (state) => {
        return state.summaryText;
    },
    contentChanged: (state) => {
        return state.contentChanged;
    }
};

const mutations = {
    setEditorContent: (state, payload) => {
        state.editorContent = payload;
    },
    setSummaryText: (state, payload) => {
        state.summaryText = payload;
    },
    setContentChanged: (state, payload) => {
        state.contentChanged = payload;
    }
};

export default {
    state,
    getters,
    mutations
}


