import Vue from 'vue'
import VueResource from 'vue-resource'

import jQuery from 'jquery';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import boostrap from 'bootstrap'
import '../node_modules/font-awesome/css/font-awesome.min.css'
import '../node_modules/highlight.js/styles/atom-one-dark.css'

import moment from 'moment'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

Vue.use(VueResource);
Vue.config.productionTip = false;
Vue.http.options.xhr = {withCredentials: true};
// Vue.http.options.root = 'https://localhost:8443'; // TODO
window.moment = moment;

// Progress bar
NProgress.configure({
    minimum: 0.15,
    easing: 'ease',
    speed: 500,
    showSpinner: false
});
Vue.http.interceptors.push((request, next) => {
    NProgress.start();
    next((resp) => {
        NProgress.done();
    }, (err) => { // TODO: Is this necessary?
        NProgress.done();
    });
});

export const eventBus = new Vue({
    methods: {

    }
});

export const global = {
    cvISO8601ToFaithDate(date) {
        return moment(date, moment.ISO_8601).format('MMM DD YYYY');
    },
    cvFaithDateToISO8601(date) {
        return moment(date, 'MMM DD YYYY').toISOString();
    },
    cvISO8601ToFaithDateTime(date) {
        return moment(date, moment.ISO_8601).format('MMM DD YYYY @ HH:mm');
    },
    cvFaithDateTimeToISO8601(date) {
        return moment(date, 'MMM DD YYYY @ HH:mm').toISOString();
    },
    cvISO8601ToUSDate(date) {
        return moment(date, moment.ISO_8601).format('MM/DD/YYYY');
    },
    cvUSDateToISO8601(date) {
        return moment(date, 'MM/DD/YYYY').toISOString();
    },
};

import Index from './Index'
import {store} from './store/store.js';

new Vue({
    el: '#app',
    store, // Global Vuex instance
    // router,
    template: '<Index/>',
    components: {Index}
});
