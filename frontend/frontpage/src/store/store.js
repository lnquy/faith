import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

// import ActivePostIdx from './modules/active_post_idx';
// import IndexScrollPos from './modules/index_scroll_pos';

export const store = new Vuex.Store({
    modules: {
        // ActivePostIdx,
        // IndexScrollPos,
    }
});
