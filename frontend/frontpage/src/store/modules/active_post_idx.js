/**
 * Created by rain on 05/05/17.
 */
const state = {
    activePostIdx: 0
};

const getters = {
    activePostIdx: (state) => {
        return state.activePostIdx;
    }
};

const mutations = {
    setActivePostIdx: (state, payload) => {
        state.activePostIdx = payload;
    }
};

export default {
    state,
    getters,
    mutations
}


