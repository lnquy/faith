/**
 * Created by rain on 05/07/17.
 */
const state = {
    indexScrollPos: 0
};

const getters = {
    indexScrollPos: (state) => {
        return state.indexScrollPos;
    }
};

const mutations = {
    setIndexScrollPos: (state, payload) => {
        state.indexScrollPos = payload;
    }
};

export default {
    state,
    getters,
    mutations
}


