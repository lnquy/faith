# Faith

Faith is a personal CMS inspired by [CleanBlog](https://github.com/BlackrockDigital/startbootstrap-clean-blog), [Ghost](https://ghost.io) and [Medium](https://medium.com).  
Built in love with [Go](https://github.com/golang/go), [VueJS](https://vuejs.org/) and [SemanticUI](https://semantic-ui.com/).

##### Get SSL certificate from Letencrypt on GCP
```
// Install certbot
$ sudo apt-get install certbot -t jessie-backports
// Get SSL certificate
$ sudo certbot certonly --standalone -d lnquy.com -d www.lnquy.com

// Move certificate and private key to Faith working directory
$ sudo cp /etc/letsencrypt/live/lnquy.com/fullchain.pem /home/lnquy_it/faith/config/certs/letencrypt/server.crt
$ sudo cp /etc/letsencrypt/live/lnquy.com/privkey.pem /home/lnquy_it/faith/config/certs/letencrypt/server.key

// Start Faith server with Letencrypt SSL certificate
$ sudo ./faith -run=production -ip=xxxx -port=443 -https=true -tlscert=/home/lnquy_it/faith/config/certs/letencrypt/server.crt -tlskey=/home/lnquy_it/faith/config/certs/letencrypt/server.key &

// Renew Letencrypt certificate (each 3 months)
$ sudo certbot renew
```
